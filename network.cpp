//
//  network.cpp
//  mobile_money
//
//  Created by Sophie Daly on 16/07/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#include "network.h"


namespace MobileMoney {
    //city names start with 1 not 0
    int count=1;
    
    Network::Network(int rank, std::mt19937& s){
        //counter used for agent ids
        ctr=new Counter();
        city_no=(rank*no_cities)+count;
        count++;
        
        //add clients
        for (int i=0; i<no_clients; i++){
            network.push_back(Client(city_no, s));
            network[i].set_id()=i;
            ctr->increment();
        }
        
        //add mules
        for (int j=0; j<no_mules; j++){
            network.push_back(Mule(city_no, s));
            network[j+no_clients].set_id()=ctr->counter;
            ctr->increment();
        }
        
        //add fraudsters
        for (int k=0; k<no_fraudsters; k++){
            network.push_back(Fraudster(city_no, s));
            network[k+no_clients+no_mules].set_id()=ctr->counter;
            ctr->increment();
        }
    }
    
    //find nearest neighbour
    coord_s nearest_neighbour(int p, int x, int y, std::mt19937& s){
        coord_s new_coord;
        //default process
        new_coord.p=p;
        
        //4 possible nearest neighbours
        int r = random_no(4, s);
        switch(r) {
            case 0:
                //up
                new_coord.x=(x+xgrid-1)%xgrid;
                new_coord.y=y;
                if(x==0)
                    new_coord.p=(p-ypgrid+no_procs)%no_procs;
                break;
            case 1:
                //down
                new_coord.x=(x+1)%xgrid;
                new_coord.y=y;
                if(x==xgrid-1)
                    new_coord.p=(p+ypgrid)%no_procs;
                break;
            case 2:
                //left
                new_coord.x=x;
                new_coord.y=(y+ygrid-1)%ygrid;
                if(y==0){
                    if(p%ypgrid==0) new_coord.p=p+(ypgrid-1);
                    else new_coord.p=p-1;
                }
                break;
            case 3:
                //right
                new_coord.x=x;
                new_coord.y=(y+1)%ygrid;
                if(y==xgrid-1){
                    if(p%ypgrid==(ypgrid-1)) new_coord.p=p-(ypgrid-1);
                    else new_coord.p=p+1;
                }
                break;
        }

        return new_coord;
    }
}