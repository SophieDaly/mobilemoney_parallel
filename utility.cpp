//
//  utility.cpp
//  mobile_money
//
//  Created by Sophie Daly on 20/06/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#include "utility.h"

//global counters
int no_susp=0;
int no_trans=0;

//generate separate seed on each proc
long generate_seed(int rank){
    long s=27531;
    long seed=std::abs(((s*181)*((rank-83)*359))%104729);
    return seed;
}


//random integer [0,max)
int random_no(int max, std::mt19937& seed){
    std::uniform_int_distribution<int> uni(0,max-1);
    return uni(seed);
}


//random double [0,1)
double random_prob(std::mt19937& seed){
    std::uniform_real_distribution<double> dis(0.0,1.0);
    return dis(seed);
}


//gaussian RNG
//assume all amounts are +ve
double random_gauss(double mean, double var, std::mt19937& seed){
    std::normal_distribution<double> nd(mean, var);
    return fabs(nd(seed));
}


void shuffle_vec(std::vector<int> &vec, std::mt19937& seed){
    std::shuffle(vec.begin(), vec.end(), seed);
}


//shuffle vec excluding 'pos' term
//total>size
void shuffle_norepeat(std::vector<int> &my_vec, int pos, int total, std::mt19937& seed){
    std::vector<int> numbers(total-1);
    for (int i=0; i<total; i++)
        if (i!=pos) numbers[i]=i;

    shuffle_vec(numbers, seed);
    //add new random values to same_city vector
    int size=my_vec.size();
    for (int j=0;j<size;j++){
        my_vec[j]=numbers[j];
    }
}


//shuffle vec allowing repeats
//total>size
void shuffle_repeat(std::vector<int> &my_vec, int total, std::mt19937& seed){
    std::vector<int> numbers(total);
    for (int i=0; i<total; i++)
        numbers[i]=i;
    
    shuffle_vec(numbers, seed);
    //add new random values to ml_mules vector
    int size=my_vec.size();
    for (int j=0; j<size; j++){
        my_vec[j]=numbers[j];
    }
}


//current date & time YYYY-MM-DD.HH:mm:ss
const std::string current_date_time(){
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d/%m/%Y %X", &tstruct);
    
    return buf;
}


