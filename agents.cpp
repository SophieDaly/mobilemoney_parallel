//
//  agents.cpp
//  mobile_money
//
//  Created by Sophie Daly on 18/06/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#include "agents.h"


namespace MobileMoney { 
    
    Index::Index(int _p, int _i, int _j, int _k){
        p=_p;
        i=_i;
        j=_j;
        k=_k;
    }
    
    bool Index::is_client(){
        if (k<no_clients) return true;
        else return false;
    }
    
    bool Index::is_mule(){
        if (k>=no_clients && k<no_clients+no_mules) return true;
        else return false;
    }
    
    bool Index::is_fraudster(){
        if (k>=no_clients+no_mules && k<no_clients+no_mules+no_fraudsters) return true;
        else return false;
    }
    
    agent_t Index::agent_type(){
        if (is_client()) return CLIENT;
        else if (is_mule()) return MULE;
        else return FRAUDSTER;
    }
    
    
    Agent::Agent(agent_t agent_type, int city_no, std::mt19937& s){
        id_no=0;
        city=city_no;
        age=set_age(s);
        balance=set_balance(age, s);
        max_balance=set_max_bal(age);
        type=agent_type;
    }
    
    bool Agent::is_client(){
        return type==CLIENT;
    }
    
    bool Agent::is_mule(){
        return type==MULE;
    }
    
    bool Agent::is_fraudster(){
        return type==FRAUDSTER;
    }
    
    
    //connections
    void Agent::connect(int p, int i, int j, int k){
        Index other_agent=Index(p, i, j, k);
        friends.push_back(other_agent);
    }
    
    void Agent::connect_mule(int p, int i, int j, int k){
        Index some_mule=Index(p, i, j, k);
        mules.push_back(some_mule);
    }
    
    void Agent::connect_fraudster(int p, int i, int j, int k){
        Index some_fraudster=Index(p, i, j, k);
        fraudsters.push_back(some_fraudster);
    }
    
    
    //select transaction operation
    trans_t Agent::select_operation(std::mt19937& s){
        trans_t operation=NOTRANS; //set as defaultt

        //decide transaction based on balance
        double bal_ratio=balance/max_balance;
        double p=random_prob(s);
            
        //decide which trans is more likely depending on balance ratio
        if (bal_ratio<0.15){
            if(p<0.05)
                operation=WITHDRAW;
            else if (p<0.1)
                operation=TRANSFER;
            else if (p<0.5)
                operation=DEPOSIT;
                
        }else if (bal_ratio<0.8){
            if(p<0.1)
                operation=DEPOSIT;
            else if (p<0.3)
                operation=WITHDRAW;
            else if (p<0.6)
                operation=TRANSFER;
                
        }else {
            if(p<0.001)
                operation=DEPOSIT;
            else if (p<0.4)
                operation=WITHDRAW;
            else if (p<0.7)
                operation=TRANSFER;
        }

        return operation;
    }
    
    
    //amount depends on transaction and age of agent and max val of transaction
    double Agent::select_amount(trans_t operation, std::mt19937& s){
        double max_val=operation_maxval(operation, s);
        double amount=0.0;
        double age_factor=0.0;
        double limit=fabs(max_balance-balance);
        switch (age){
            case LOW:
                age_factor=0.5;
                break;
            case MEDIUM:
                age_factor=0.8;
                break;
            case HIGH:
                age_factor=1;
                break;
            default:
                break;
        }

        //find amount based on age_factor
        //min between limit & transaction max_val
        if(limit>max_val)
            limit=max_val;
        
        if(limit>=1)
            amount=limit*age_factor;
        
        return amount;
    }
    
    
    void Agent::deposit(std::vector<Transaction> &records, int step, std::mt19937& s){
        double amount=select_amount(DEPOSIT, s);
        increment_bal(amount);
        
        //record transaction & check if suspicious
        Transaction transaction=Transaction(step, age , type, city, DEPOSIT, amount, balance);
        transaction.is_suspicious();
        records.push_back(transaction);
    }
    
    
    void Agent::withdraw(std::vector<Transaction> &records, int step, std::mt19937& s){
        double amount=select_amount(WITHDRAW, s);
        decrement_bal(amount);
        
        //record transaction & check if suspicious
        Transaction transaction=Transaction(step, age , type, city, WITHDRAW, -amount, balance);
        transaction.is_suspicious();
        records.push_back(transaction);
    }

    
    //for every transfer there is also a deposit transaction with the same amount for the receiving agent
    void Agent::transfer(std::vector<Transaction> &records, Agent recv_agent, double amount, int step){
        decrement_bal(amount);
        double old_balB=recv_agent.balance;
        recv_agent.increment_bal(amount);
        
        //transfer details
        age_t ageB=recv_agent.age;
        agent_t typeB=recv_agent.type;
        int cityB=recv_agent.city;
        double balB=recv_agent.balance;
        
        //record transaction
        Transaction transactionA=Transaction(step, age, type, city, TRANSFER, -amount, balance, typeB, cityB, old_balB);
        transactionA.is_suspicious();
        records.push_back(transactionA);
        
        //record receiving transaction
        Transaction transactionB=Transaction(step, ageB, typeB, cityB, TRANSFER, amount, balB, type, city, balance);
        transactionA.is_suspicious();
        records.push_back(transactionB);
    }
    
    
    //placement
    void Agent::deposit_dirty(std::vector<Transaction> &records, double dirty_money, int step){
        increment_bal(dirty_money);
        
        //record transaction & check if suspicious
        Transaction transaction=Transaction(step, age, type, city, DEPOSIT, dirty_money, balance);
        transaction.is_suspicious();
        records.push_back(transaction);
    }
    
    
    //integration
    void Agent::withdraw_laundered(std::vector<Transaction> &records, double laundered_money, int step){
        decrement_bal(laundered_money);
        
        //record transaction & check if suspicious
        Transaction transaction=Transaction(step, age , type, city, WITHDRAW, laundered_money, balance);
        transaction.is_suspicious();
        records.push_back(transaction);
    }
    
}


MobileMoney::age_t set_age(std::mt19937& s){
    double p=random_prob(s);
    MobileMoney::age_t age_type;
    if(p<0.2)
        age_type=MobileMoney::LOW;
    else if(p<0.6)
        age_type=MobileMoney::MEDIUM;
    else
        age_type=MobileMoney::HIGH;
    
    return age_type;
}


double set_balance(int age, std::mt19937& s){
    double bal=0.0;
    switch (age) {
        case MobileMoney::LOW:
            bal=random_gauss(low_mean, low_var, s);
            break;
        case MobileMoney::MEDIUM:
            bal=random_gauss(med_mean, med_var, s);
            break;
        case MobileMoney::HIGH:
            bal=random_gauss(high_mean, high_var, s);
            break;
        default:
            break;
    }
    return bal;
}


double set_max_bal(int age){
    double max_bal=0.0;
    switch (age) {
        case MobileMoney::LOW:
            max_bal=max_low;
            break;
        case MobileMoney::MEDIUM:
            max_bal=max_med;
            break;
        case MobileMoney::HIGH:
            max_bal=max_high;
            break;
        default:
            break;
    }
    return max_bal;
}


//dirty money amount to be laundered
double select_dirty(std::mt19937& s){
    return dirty*random_gauss(5, 1, s);
}


//return max_val of specific operation
double operation_maxval(MobileMoney::trans_t operation, std::mt19937& s){
    double max_val=0.0;
    switch (operation){
        case MobileMoney::DEPOSIT:
            max_val=1500*random_gauss(0.0, 1.0, s);
            break;
        case MobileMoney::WITHDRAW:
            max_val=1200*random_gauss(0.0, 1.0, s);
            break;
        case MobileMoney::TRANSFER:
            max_val=1000*random_gauss(0.0, 1.0, s);
            break;
        default:
            break;
    }
    return max_val;
}


void log_transactions(std::vector<MobileMoney::Transaction> &records, std::ofstream &outfile){
    for(auto it=records.begin(); it!=records.end(); it++)
        it->get_record(outfile);
    
}
