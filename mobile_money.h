//
//  mobile_money.h
//  mobile_money
//
//  Created by Sophie Daly on 02/08/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#ifndef __mobile_money__mobile_money__
#define __mobile_money__mobile_money__

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "network.h"
#include "agents.h"
#include "transactions.h"


//print closing message
void print_results(std::ofstream &outfile, int np, double time);

//connections
void make_connections(std::vector<std::vector<MobileMoney::Network> > &city, int rank, std::mt19937& s);
void make_illegal_connections(std::vector<std::vector<MobileMoney::Network> > &city, int rank, std::mt19937& s);

//simulation step
void step(MobileMoney::Agent agent, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<std::vector<MobileMoney::Message> > &messages, std::vector<MobileMoney::Transaction> &records, int rank, int n, std::mt19937& s);

//legal activity
void legal_activity(MobileMoney::Agent agent, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<std::vector<MobileMoney::Message> > &messages, std::vector<MobileMoney::Transaction> &records, int rank, int step, std::mt19937& s);
void implement_transfer(MobileMoney::Agent agent, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<std::vector<MobileMoney::Message> > &messages, std::vector<MobileMoney::Transaction> &records, int rank, int step, std::mt19937& s);

//illegal activity
void fraudster_activity(MobileMoney::Agent agent, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<std::vector<MobileMoney::Message> > &messages, std::vector<MobileMoney::Transaction> &records, int rank, int step, std::mt19937& s);
void illegal_activity(MobileMoney::Agent fraudster, std::vector<std::vector<MobileMoney::Message> > &messages, std::vector<MobileMoney::Transaction> &records, int step, std::mt19937& s);
void layering_step(MobileMoney::Agent fraudster, MobileMoney::Index mule, MobileMoney::Index recv_fraudster, double mule_amount, std::vector<std::vector<MobileMoney::Message> > &messages, int step);
void integration_step(MobileMoney::Index receiver, double amount, std::vector<std::vector<MobileMoney::Message> > &messages, int step);

//implement transaction from message passed between processes
void implement_transaction(MobileMoney::Message message, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<MobileMoney::Transaction> &records);
void implement_transferA(MobileMoney::Message message, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<MobileMoney::Transaction> &records);
void implement_transferB(MobileMoney::Message message, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<MobileMoney::Transaction> &records);
void implement_withdrawal(MobileMoney::Message message, std::vector<std::vector<MobileMoney::Network> > &city, std::vector<MobileMoney::Transaction> &records);
void accept_transfer(MobileMoney::Message message, std::vector<std::vector<MobileMoney::Network> > &city);


#endif /* defined(__mobile_money__mobile_money__) */
