//
//  utility.h
//  mobile_money
//
//  Created by Sophie Daly on 20/06/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#ifndef __mobile_money__utility__
#define __mobile_money__utility__

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <algorithm>
#include <chrono>
#include <time.h>

//processes
#define xpgrid 4
#define ypgrid 4
#define no_procs xpgrid*ypgrid

//per process
//grid values
#define xgrid 8
#define ygrid 5
#define no_cities xgrid*ygrid

//per city
#define no_agents 100
#define no_clients 70
#define no_mules 20
#define no_fraudsters 10

#define total_cities no_procs*no_cities
#define total_agents no_agents*total_cities
#define total_clients no_clients*total_cities
#define total_mules no_mules*total_cities
#define total_fraudsters no_fraudsters*total_cities

//simulation
#define no_sims 100

//agent connections
#define same_friends 5
#define diff_friends 2
#define total_friends 7
#define no_mules_set 4
#define no_fraudsters_set 4

//balance info
#define low_mean 5000.0
#define low_var 1000.0
#define med_mean 15000.0
#define med_var 5000.0
#define high_mean 25000.0
#define high_var 10000.0
#define max_low 10000.0
#define max_med 30000.0
#define max_high 50000.0

//illegal activity info
#define trans_rate 0.5
#define fraud_rate 0.1
#define dirty 10000
#define mule_fee 0.01

//global counters
extern int no_susp;
extern int no_trans;

long generate_seed(int rank);
int random_no(int max, std::mt19937& seed);
double random_prob(std::mt19937& seed);
double random_gauss(double min, double max, std::mt19937& seed);
void shuffle_vec(std::vector<int> &vec, std::mt19937& seed);
void shuffle_norepeat(std::vector<int> &my_vec, int pos, int total, std::mt19937& seed);
void shuffle_repeat(std::vector<int> &my_vec, int total, std::mt19937& seed);
const std::string current_date_time();

#endif /* defined(__mobile_money__utility__) */
