//
//  transactions.cpp
//  mobile_money
//
//  Created by Sophie Daly on 15/07/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#include "transactions.h"


namespace MobileMoney {
    
    //DEPOSIT & WITHDRAWAL
    Transaction::Transaction(int _step, age_t _age, agent_t _type, int _city, trans_t _trans, double _amount, double _balance){
        
        step=_step;
        age=_age;
        agent_type=_type;
        city=_city;
        trans_type=_trans;
        amount=_amount;
        new_balance=_balance;
        agent_typeB=NOAGENT;
        cityB=0;
        new_balanceB=0.0;
        suspicious=false;
        
    }
    
    
    //TRANSFER
    Transaction::Transaction(int _step, age_t _age, agent_t _type, int _city, trans_t _trans, double _amount, double _balance, agent_t _typeB, int _cityB, double _balanceB){
        
        step=_step;
        age=_age;
        agent_type=_type;
        city=_city;
        trans_type=_trans;
        amount=_amount;
        new_balance=_balance;
        agent_typeB=_typeB;
        cityB=_cityB;
        new_balanceB=_balanceB;
        suspicious=false;
    }
    
    
    //check if suspicious & increment counters
    void Transaction::is_suspicious(){
        no_trans++;
        //if transaction amount surpasses limit -> set suspicious flag true
        if (fabs(amount)>dirty){
            set_susp()=true;
            no_susp++;
        }
    }
    
    
    //print transaction log
    void Transaction::get_record(std::ofstream &outfile){
        
        outfile << step << ",";
        outfile << age << ",";
        switch (agent_type) {
            case CLIENT:
                outfile<< "P1,";
                break;
            case MULE:
                outfile<< "P2,";
                break;
            case FRAUDSTER:
                outfile<< "P3,";
                break;
            default:
                break;
        }
        outfile << city << ",";
        outfile << trans_type << ",";
        outfile << amount << ",";
        outfile << new_balance << ",";
        
        //transfer params
        switch (agent_typeB) {
            case NOAGENT:
                outfile<< "null,";
                break;
            case CLIENT:
                outfile<< "P1,";
                break;
            case MULE:
                outfile<< "P2,";
                break;
            case FRAUDSTER:
                outfile<< "P3,";
                break;
        }
        
        if(cityB==0) outfile<< "null,";
        else outfile<< cityB <<",";
        outfile << new_balanceB << ",";
        outfile << suspicious <<std::endl;
    }

}


//header needed for Machine Learning ARFF file
void print_header(std::ofstream &outfile){
    outfile<< "@RELATION 'MobileMoney'" <<std::endl;
    outfile<< "" <<std::endl;
    outfile<< "@ATTRIBUTE step NUMERIC" <<std::endl;
    outfile<< "@ATTRIBUTE age {1,2,3}" <<std::endl;
    outfile<< "@ATTRIBUTE agentA {P1,P2,P3}" <<std::endl;
    
    outfile<< "@ATTRIBUTE city {";
    for(int i=1;i<total_cities;i++) outfile<< i << ",";
    outfile<< total_cities << "}" <<std::endl;
    
    outfile<< "@ATTRIBUTE trans_type {1,2,3}" <<std::endl;
    outfile<< "@ATTRIBUTE amount NUMERIC" <<std::endl;
    outfile<< "@ATTRIBUTE balance NUMERIC" <<std::endl;
    outfile<< "@ATTRIBUTE agentB {P1,P2,P3,null}" <<std::endl;
    
    outfile<< "@ATTRIBUTE cityB {null,";
    for(int i=1;i<total_cities;i++) outfile<< i << ",";
    outfile<< total_cities << "}" <<std::endl;
    
    outfile<< "@ATTRIBUTE balanceB NUMERIC" <<std::endl;
    outfile<< "@ATTRIBUTE suspicious {0,1}" <<std::endl;
    outfile<< "" <<std::endl;
    outfile<< "@DATA" <<std::endl;
}

