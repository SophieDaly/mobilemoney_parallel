//
//  main.cpp
//  mobile_money
//
//  Money Laundering Detection using Multi Agent Based Simulation
//
//  Parallelised using Boost MPI
//
//  Created by Sophie Daly on 18/06/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include "agents.h"
#include "network.h"
#include "mobile_money.h"
#include "transactions.h"
#include "utility.h"

namespace mpi=boost::mpi;

int main(int argc, char * argv[]) {
    //start timer
    auto begin = std::chrono::high_resolution_clock::now();
    
    //boost mpi
    mpi::environment env(argc, argv);
    mpi::communicator world;
    int rank=world.rank();
    int np=world.size();
    
    //seed rng once
    long my_seed=generate_seed(rank);
    std::mt19937 s;
    s.seed(my_seed);
   
    //change to suitable dir
    std::string path="/home/users/mschpc/2014/dalyso/Project/Parallel/mobile_money/output/";

    //open files
    std::string results="results.txt";
    std::string logs="logs"+std::to_string(rank)+".arff";
    std::ofstream outfile1(path+results);
    std::ofstream outfile2(path+logs);
    if (!outfile1.is_open()) std::cout<< "Unable to open file1" <<std::endl;
    if (!outfile2.is_open()) std::cout<< "Unable to open file2" <<std::endl;

    //print details
    if (rank==0){
        print_header(outfile2);
    }

    //initialize mobile network
    std::vector<std::vector<MobileMoney::Network> > city;
    city.resize(xgrid);
    for (int i=0; i<xgrid; i++) {
        for (int j=0; j<ygrid; j++) {
            city[i].push_back(MobileMoney::Network(rank, s));
        }
    }
    
    //add connections
    make_connections(city, rank, s);
    make_illegal_connections(city, rank, s);

    
    //run simulation
    for (int n=0; n<no_sims; n++){
        
        //create matrix to hold messages that will be sent between processes
        std::vector<std::vector<MobileMoney::Message> > messages, recvs;
        messages.resize(np);
        recvs.resize(np);
        
        //create vector to hold transactions
        std::vector<MobileMoney::Transaction> records;
        
        for (int i=0; i<xgrid; i++)
            for (int j=0; j<ygrid; j++)
                for (int k=0; k<no_agents; k++)
                    step(city[i][j].network[k], city, messages, records, rank, n, s);


        //send message[i] to process i
        all_to_all(world, messages, recvs);
        world.barrier();
     
        //carry out transactions
        for (int p=0; p<np; p++){
            int size=recvs[p].size();
            if (size>0){
                for (int x=0; x<size; x++)
                    implement_transaction(recvs[p][x], city, records);
            }
        }
        world.barrier();
        log_transactions(records, outfile2);
    }
    
    
    //end timer
    auto end=std::chrono::high_resolution_clock::now();
    double time=std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() ;

    //print results
    if (rank==0)
        print_results(outfile1, np, time);
   
    //close files
    outfile1.close();
    outfile2.close();
    
    
    return 0;
}

