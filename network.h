//
//  network.h
//  mobile_money
//
//  Created by Sophie Daly on 16/07/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#ifndef __mobile_money__network__
#define __mobile_money__network__

#include <vector>
#include "agents.h"

namespace MobileMoney{
    
    class Counter{
        
    public:
        int counter;
        Counter() {counter=0;}
        int get_ctr() {return counter;}
        void increment() {counter++;}
        void decrement() {if (counter>0) counter--;}
        bool isZero() {if (counter== 0) return 1; return 0;}
        
    };
    
    
    class Network{
        
    public:
        //each network is a vec of agents
        std::vector<Agent> network;
        Counter *ctr;
        int city_no;
        
        Network(int rank, std::mt19937& s);
        
        ~Network(){
            if (ctr) {
                if (ctr->isZero()) {
                    delete ctr;
                    if (!network.empty()) network.clear();
                }
                else ctr->decrement();
            }
        }
        
        int get_city() {return city_no;}
    };
    
    //struct to hold city coords
    struct coord_s{
        int p;
        int x;
        int y;
    };
    
    //find nearest neighbour
    coord_s nearest_neighbour(int p, int x, int y, std::mt19937& s);
    
}

#endif /* defined(__mobile_money__network__) */
