CC = mpic++
CXXFLAGS =-g -std=c++0x -Wall -W -Werror 
LIBS = -lboost_mpi -lboost_serialization

all: mobilemoney

mobilemoney: main.o agents.o mobile_money.o network.o transactions.o utility.o
	$(CC) $(CXXFLAGS) main.o agents.o mobile_money.o network.o transactions.o utility.o -o mobilemoney $(LIBS) 

clean:
	rm *o mobilemoney 
