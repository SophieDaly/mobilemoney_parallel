//
//  transactions.h
//  mobile_money
//
//  Created by Sophie Daly on 15/07/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#ifndef __mobile_money__transactions__
#define __mobile_money__transactions__

#include <iostream>
#include <fstream>
#include <boost/mpi.hpp>
#include "utility.h"


namespace MobileMoney {
    enum agent_t {NOAGENT=0, CLIENT=1, MULE=2, FRAUDSTER=3};
    enum age_t {NOAGE=0, LOW=1, MEDIUM=2, HIGH=3};
    enum trans_t {NOTRANS=0, DEPOSIT=1, WITHDRAW=2, TRANSFER=3, TRANSFERB=4};
    
    class Transaction{
        
    public:
        int step;
        age_t age;
        agent_t agent_type;
        int city;
        trans_t trans_type;
        double amount;
        double new_balance;
        agent_t agent_typeB;
        int cityB;
        double new_balanceB;
        bool suspicious;
        
        Transaction() {};
        
        //DEPOSIT & WITHDRAWAL 
        Transaction(int _step, age_t _age, agent_t _type, int _city, trans_t _trans, double _amount, double _balance);
        
        //TRANSFER
        Transaction(int _step, age_t _age, agent_t _type, int _city, trans_t _trans, double _amount, double _balance, agent_t _typeB, int _cityB, double _balanceB);
        
        //set suspicious flag
        bool& set_susp() {return suspicious;}
        
        //check if transaction is suspicious
        void is_suspicious();

        //print transaction log
        void get_record(std::ofstream &outfile);
    };
    
    
    //message will be passed between processes for transfer transactions
    class Message{
    private:
        friend class boost::serialization::access;
        
        template<class Archive>
        void serialize(Archive &ar, const unsigned int /*version*/){
            ar & p;
            ar & i;
            ar & j;
            ar & k;
            ar & step;
            ar & age;
            ar & agent_type;
            ar & city;
            ar & trans_type;
            ar & amount;
            ar & balance;
        }
        
    public:
        int p;
        int i;
        int j;
        int k;
        int step;
        age_t age;
        agent_t agent_type;
        int city;
        trans_t trans_type;
        double amount;
        double balance;
        
        Message() {};
        
        //WITHDRAWAL
        Message(int _p, int _i, int _j, int _k, int _step, trans_t type, double _amount) :
        p(_p), i(_i), j(_j), k(_k), step(_step), trans_type(type), amount(_amount)
        {}
        
        //TRANSFER
        Message(int _p, int _i, int _j, int _k, int _step, age_t _age, agent_t _type, int _city, trans_t _trans, double _amount, double _bal) :
        p(_p), i(_i), j(_j), k(_k), step(_step), age(_age), agent_type(_type), city(_city), trans_type(_trans), amount(_amount), balance(_bal)
        {}

    };
    
}

//header needed for Machine Learning ARFF file
void print_header(std::ofstream &outfile);


#endif /* defined(__mobile_money__transaction__) */


