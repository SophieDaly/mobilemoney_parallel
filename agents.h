//
//  agents.h
//  mobile_money
//
//  Created by Sophie Daly on 18/06/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#ifndef __mobile_money__agents__
#define __mobile_money__agents__

#include <vector>
#include "utility.h"
#include "transactions.h"

namespace MobileMoney{
    
    //object to hold city coords and index no of each agent
    class Index{
        
    protected:
        int p;
        int i;
        int j;
        int k;
        
    public:
        Index(int _p, int _i, int _j, int _k);
        
        int get_p() const {return p;}
        int get_i() const {return i;}
        int get_j() const {return j;}
        int get_k() const {return k;}
        
        agent_t agent_type();
        bool is_client();
        bool is_mule();
        bool is_fraudster();
    };
    
    
    class Agent{
        
    protected:
        int id_no;
        int city;
        double balance;
        double max_balance;
        age_t age;
        agent_t type;
        
    public:
        std::vector<Index> friends;
        //vecs needed for fraudulant activity
        std::vector<Index> mules;
        std::vector<Index> fraudsters;
        
        Agent(int id_no, int city, double bal, agent_t agent_type):
        id_no(id_no), city(city), balance(bal), type(agent_type) {}
        
        Agent(agent_t agent_type, int city_no, std::mt19937& s);
        
        int get_id() const {return id_no;}
        int& set_id() {return id_no;}
        int get_city() const {return city;}
        double get_bal() const {return balance;}
        age_t get_age() const {return age;}
        agent_t get_type() const {return type;}
        bool is_client();
        bool is_mule();
        bool is_fraudster();
        void increment_bal(double amount) {balance+=amount;}
        void decrement_bal(double amount) {balance-=amount;}
        
        //connect with other agents
        void connect(int p, int x, int y, int k);
        void connect_mule(int p, int x, int y, int k);
        void connect_fraudster(int p, int x, int y, int k);
        
        //transaction fns
        trans_t select_operation(std::mt19937& s);
        double select_amount(trans_t operation, std::mt19937& s);
        
        void deposit(std::vector<Transaction> &records, int step, std::mt19937& s);
        void withdraw(std::vector<Transaction> &records, int step, std::mt19937& s);
        void transfer(std::vector<Transaction> &records, Agent recv_agent, double amount, int step);
        
        //illegal activity
        void deposit_dirty(std::vector<Transaction> &records, double dirty_money, int step);
        void withdraw_laundered(std::vector<Transaction> &records, double laundered_money, int step);
        
    };
    
    
    class Client: public Agent{
    public:
        Client(int city, std::mt19937& s): Agent(CLIENT, city, s) {}
    };
    
    class Mule: public Agent{
    public:
        Mule(int city, std::mt19937& s): Agent(MULE, city, s) {}
    };
    
    class Fraudster: public Agent{
    public:
        Fraudster(int city, std::mt19937& s): Agent(FRAUDSTER, city, s) {}
    };
    
}

MobileMoney::age_t set_age(std::mt19937& s);
double set_balance(int age, std::mt19937& s);
double set_max_bal(int age);
double select_dirty(std::mt19937& s);
double operation_maxval(MobileMoney::trans_t operation, std::mt19937& s);
void log_transactions(std::vector<MobileMoney::Transaction> &records, std::ofstream &outfile);


#endif /* defined(__mobile_money__agent__) */
