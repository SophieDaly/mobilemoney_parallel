#!/bin/bash

#SBATCH --reservation=application
#SBATCH -n 32           
#SBATCH -p compute
#SBATCH -t 01:30:00     
#SBATCH -U mschpc
#SBATCH -J mm_32

# source the module commands
source /etc/profile.d/modules.sh

# load the modules used to build the xhpl binary
module load apps libs cports boost/gcc/4.8.2/openmpi/1.6.5/64/1.55 default-gcc-openmpi-4.8.2-1.6.5

# run it    
mpirun -npernode 8 ./mobilemoney


