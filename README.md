# README #

Money Laundering Detection in Mobile Transactions using Multi Agent Based Simulation

Project submitted in partial fulfilment of the requirements for MSc. High Performance Computing TCD

### What is this repository for? ###

* Multi Agent Based Simulation designed to emulate the behaviour of agents interacting in a Mobile Money environment

* Programmed in C++
* Parallelised using Boost MPI

### How do I get set up? ###

* requires C++11
* command to load modules:
"module load apps libs cports boost/gcc/4.8.2/openmpi/1.6.5/64/1.55 default-gcc-openmpi-4.8.2-1.6.5"
*
* define macros in utility.h need to be carefully set with #processes etc. in order to run without error
* file path in main.cpp needs to be set to save output to correct directory