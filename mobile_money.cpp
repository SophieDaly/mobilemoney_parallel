//
//  mobile_money.cpp
//  mobile_money
//
//  Created by Sophie Daly on 02/08/2015.
//  Copyright (c) 2015 Sophie Daly. All rights reserved.
//

#include "mobile_money.h"

using namespace MobileMoney;


void print_results(std::ofstream &outfile, int np, double time){
    int mins=(time*1e-9)/60;
    int secs=time*1e-9 - mins*60.;
    float milli=time*1e-6 - (mins*60000.0 + secs*1000.0);
    double percent=no_susp*100.0/no_trans;
    
    outfile<< "*******************************************************\n \t\tMOBILE MONEY\n*******************************************************" <<std::endl;
    outfile<< "   Money Laundering Detection in Mobile Transactions\n         using Multi Agent Based Simulation" <<std::endl;
    outfile<< "_______________________________________________________\n" << std::endl;
    outfile<< " date: " << current_date_time() << std::endl;
    outfile<< " #processes: " << np <<std::endl;
    outfile<< " #sims: " << no_sims <<std::endl;
    outfile<< " #cities: " << total_cities <<std::endl;
    outfile<< " #agents: " << total_agents <<std::endl;
    outfile<< "_______________________________________________________\n" <<std::endl;
    outfile<< " #cities/process: " << no_cities <<std::endl;
    outfile<< "\n #clients/city: " << no_clients <<std::endl;
    outfile<< " #mules/city: " << no_mules <<std::endl;
    outfile<< " #fraudsters/city: " << no_fraudsters <<std::endl;
    outfile<< "_______________________________________________________\n" <<std::endl;
    outfile<< " #transactions: " << no_trans <<std::endl;
    outfile<< " #suspicious: " << no_susp <<std::endl;
    outfile<< " %suspicious: " << percent << "%" <<std::endl;
    outfile<< "\n TIME: " << mins << "m " << secs << "s " << milli << "ms  (" << time << "ns)" <<std::endl;
    outfile<< "_______________________________________________________" << std::endl;
    
}


//connect friends to each agent
//nearest neighbour friends may be on a different process
void make_connections(std::vector<std::vector<Network> > &city, int rank, std::mt19937& s){
    for (int i=0; i<xgrid; i++){
        for (int j=0; j<ygrid; j++){
            for (auto it = city[i][j].network.begin(); it!=city[i][j].network.end(); it++){
    
                std::vector<int> same_city(same_friends), diff_city(diff_friends);
                shuffle_norepeat(same_city, it->get_id(), no_agents, s);
                shuffle_repeat(diff_city, no_agents, s);
                
                //add friends from same city
                //same city => same process
                for (int k=0; k<same_friends; k++)
                    it->connect(rank, i, j, same_city[k]);
                
                //select nearest neighbour
                coord_s new_coords=nearest_neighbour(rank, i, j, s);
                
                //add friends from diff city
                for (int l=0; l<diff_friends; l++)
                    it->connect(new_coords.p, new_coords.x, new_coords.y, diff_city[l]);
                
            }
        }
    }
}


//connect random set of mules and fraudsters to each fraudster
void make_illegal_connections(std::vector<std::vector<Network> > &city, int rank, std::mt19937& s){
    for (int i=0; i<xgrid; i++){
        for (int j=0; j<ygrid; j++){
            for (int k=no_clients+no_mules; k<no_agents; k++){
                
                //add mules
                //mules are on diff processes
                for (int l=0; l<no_mules_set; l++)
                    city[i][j].network[k].connect_mule(random_no(no_procs, s), random_no(xgrid, s), random_no(ygrid, s), random_no(no_mules, s)+no_clients);
                
                //add fraudsters
                //recv fraudster must be on same process to avoid errors with transaction logs
                for (int m=0; m<no_fraudsters_set; m++)
                    city[i][j].network[k].connect_fraudster(rank, random_no(xgrid, s), random_no(ygrid, s), random_no(no_fraudsters, s)+no_mules+no_clients);
                
            }
        }
    }
}


//implement step of simulation
//CLIENTS & MULES behave legally
//FRAUDSTERS behave illegally
void step(Agent agent, std::vector<std::vector<Network> > &city, std::vector<std::vector<Message> > &messages, std::vector<Transaction> &records, int rank, int n, std::mt19937& s){
    agent_t type=agent.get_type();
    
    switch (type) {
        case CLIENT:
            legal_activity(agent, city, messages, records, rank, n, s);
            break;
        case MULE:
            legal_activity(agent, city, messages, records, rank, n, s);
            break;
        case FRAUDSTER:
             fraudster_activity(agent, city, messages, records, rank, n, s);
            break;
        default:
            break;
    }
}


//legal activity
//implement DEPOSIT, WITHDRAW, TRANSFER or do nothing
void legal_activity(Agent agent, std::vector<std::vector<Network> > &city, std::vector<std::vector<Message> > &messages, std::vector<Transaction> &records, int rank, int step, std::mt19937& s){
    trans_t operation=agent.select_operation(s);

    switch (operation) {
        case DEPOSIT:
            agent.deposit(records, step, s);
            break;
        case WITHDRAW:
            agent.withdraw(records, step, s);
            break;
        case TRANSFER:
            implement_transfer(agent, city, messages, records, rank, step, s);
            break;
        default:
            break;
    }
}


//agent transfers amount to their friend
void implement_transfer(Agent agent, std::vector<std::vector<Network> > &city, std::vector<std::vector<Message> > &messages, std::vector<Transaction> &records, int rank, int step, std::mt19937& s){
    //select transfer amount
    double amount=agent.select_amount(WITHDRAW, s);

    //select random receiving friend
    int m=random_no(total_friends, s);
    int p=agent.friends[m].get_p();
    int i=agent.friends[m].get_i();
    int j=agent.friends[m].get_j();
    int k=agent.friends[m].get_k();
    
    //check if friend is on new process
    if (p==rank)
        agent.transfer(records, city[i][j].network[k], amount, step);
    else {
        //reduce agents balance
        agent.decrement_bal(amount);
        //create message to send to process p
        age_t ageA=agent.get_age();
        agent_t typeA=agent.get_type();
        int cityA=agent.get_city();
        double balanceA=agent.get_bal();
        messages[p].push_back(Message(p, i, j, k, step, ageA, typeA, cityA, TRANSFER, amount, balanceA));
    }
}


//fruadsters mostly act legally but sometimes illegally
void fraudster_activity(Agent agent, std::vector<std::vector<Network> > &city, std::vector<std::vector<Message> > &messages, std::vector<Transaction> &records, int rank, int step, std::mt19937& s){
    double p=random_prob(s);
    if (p>fraud_rate)
        legal_activity(agent, city, messages, records, rank, step, s);
    else
        illegal_activity(agent, messages, records, step, s);
}


void illegal_activity(Agent fraudster, std::vector<std::vector<Message> > &messages, std::vector<Transaction> &records, int step, std::mt19937& s){
    
    //PLACEMENT
    //deposit dirty money
    double dirty_money=select_dirty(s);
    fraudster.deposit_dirty(records, dirty_money, step);
    
    //LAYERING
    //divide money amongst mules
    double mule_amount=dirty_money/no_mules_set;
    
    //choose random fraudster to receive dirty money
    int p=random_no(no_fraudsters_set, s);
    
    //transfer mule_amount to each mule in mules_set who then transfers portion to receiving fraudster
    for (int i=0; i<no_mules_set; i++)
        layering_step(fraudster, fraudster.mules[i], fraudster.fraudsters[p], mule_amount, messages, step);
    
    //INTEGRATION
    //receiving fraudster withdraws laundered money
    double mule_share=mule_amount*mule_fee*no_mules_set;
    double laundered_money=dirty_money-mule_share;
    integration_step(fraudster.fraudsters[p], laundered_money, messages, step);
    
}


//LAYERING
//fraudster transfers money to mules who then transfer money to receiving fraudster (keeping share for themselves)
void layering_step(Agent fraudster, Index mule, Index recv_fraudster, double mule_amount, std::vector<std::vector<Message> > &messages, int step){

    //transfer 1: FRAUDSTER -> MULE
    int m_p=mule.get_p();
    int m_i=mule.get_i();
    int m_j=mule.get_j();
    int m_k=mule.get_k();
    
    //reduce agents balance
    fraudster.decrement_bal(mule_amount);
    //create message to send to process p
    age_t ageA=fraudster.get_age();
    int cityA=fraudster.get_city();
    double balanceA=fraudster.get_bal();
    messages[m_p].push_back(Message(m_p, m_i, m_j, m_k, step, ageA, FRAUDSTER, cityA, TRANSFER, mule_amount, balanceA));
    
    
    //transfer 2: MULE -> RECV FRAUDSTER
    //mule keeps a small fee
    double fraudster_amount=mule_amount*(1.0-mule_fee);
    int f_p=recv_fraudster.get_p();
    int f_i=recv_fraudster.get_i();
    int f_j=recv_fraudster.get_j();
    int f_k=recv_fraudster.get_k();

    //create message to send to process m_p
    //reduce mules balance and log transaction
    age_t age=fraudster.get_age();
    int city_no=fraudster.get_city();
    double balance=fraudster.get_bal();
    messages[m_p].push_back(Message(m_p, m_i, m_j, m_k, step, age, FRAUDSTER, city_no, TRANSFERB, fraudster_amount, balance));
       
    //create message to send to process f_p
    messages[f_p].push_back(Message(f_p, f_i, f_j, f_k, step, NOTRANS, fraudster_amount));
  
}


//INTEGRATION
//receiving fraudster withdraws laundered money
void integration_step(Index receiver, double amount, std::vector<std::vector<Message> > &messages, int step){
    int p=receiver.get_p();
    int i=receiver.get_i();
    int j=receiver.get_j();
    int k=receiver.get_k();
    
    messages[p].push_back(Message(p, i, j, k, step, WITHDRAW, amount));
    
}


//implement and log transaction from message passed between processes
void implement_transaction(Message message, std::vector<std::vector<Network> > &city, std::vector<Transaction> &records){
    trans_t trans_type=message.trans_type;
    
    switch (trans_type) {
        case TRANSFER:
            implement_transferA(message, city, records);
            break;
        case TRANSFERB:
            implement_transferB(message, city, records);
            break;
        case WITHDRAW:
            implement_withdrawal(message, city, records);
            break;
        case NOTRANS:
            //increase bal but no transaction record needed as taken care of in 'transfer'
            accept_transfer(message, city);
            break;
        default:
            break;
    }
}


//implement and log transaction from agent to agent on new process
void implement_transferA(Message message, std::vector<std::vector<Network> > &city, std::vector<Transaction> &records){
    
    //transaction details
    int i=message.i;
    int j=message.j;
    int k=message.k;
    double amount=message.amount;
    int step=message.step;
    age_t ageA=message.age;
    agent_t typeA=message.agent_type;
    int cityA=message.city;
    double balA=message.balance;
    age_t ageB=city[i][j].network[k].get_age();
    agent_t typeB=city[i][j].network[k].get_type();
    int cityB=city[i][j].network[k].get_city();
    double balB=city[i][j].network[k].get_bal();
    double old_balB=balB-amount;
        
    //increase agents bal
    city[i][j].network[k].increment_bal(amount);
        
    //record transaction
    Transaction transactionA=Transaction(step, ageA, typeA, cityA, TRANSFER, -amount, balA, typeB, cityB, old_balB);
    transactionA.is_suspicious();
    records.push_back(transactionA);
    
    //record receiving transaction
    Transaction transactionB=Transaction(step, ageB, typeB, cityB, TRANSFER, amount, balB, typeA, cityA, balA);
    transactionB.is_suspicious();
    records.push_back(transactionB);
    
}


//implement and log transaction from agent on new process to agent
void implement_transferB(Message message, std::vector<std::vector<Network> > &city, std::vector<Transaction> &records){
    
    //transaction details
    int i=message.i;
    int j=message.j;
    int k=message.k;
    double amount=message.amount;
    int step=message.step;
    age_t ageB=message.age;
    agent_t typeB=message.agent_type;
    int cityB=message.city;
    double balB=message.balance;
    age_t ageA=city[i][j].network[k].get_age();
    agent_t typeA=city[i][j].network[k].get_type();
    int cityA=city[i][j].network[k].get_city();
    double balA=city[i][j].network[k].get_bal();
     double old_balB=balB-amount;
    
    //reduce agents bal
    city[i][j].network[k].decrement_bal(amount);
    
    //record transaction
    Transaction transactionA=Transaction(step, ageA, typeA, cityA, TRANSFER, -amount, balA, typeB, cityB, old_balB);
    transactionA.is_suspicious();
    records.push_back(transactionA);
    
    //record receiving transaction
    Transaction transactionB=Transaction(step, ageB, typeB, cityB, TRANSFER, amount, balB, typeA, cityA, balA);
    transactionB.is_suspicious();
    records.push_back(transactionB);
    
}


//implement and log withdrawal from message passed between processes
void implement_withdrawal(Message message, std::vector<std::vector<Network> > &city, std::vector<Transaction> &records){
    
    //transaction details
    int i=message.i;
    int j=message.j;
    int k=message.k;
    double amount=message.amount;
    int step=message.step;
    age_t ageA=city[i][j].network[k].get_age();
    agent_t typeA=city[i][j].network[k].get_type();
    int cityA=city[i][j].network[k].get_city();
    double balA=city[i][j].network[k].get_bal();
    
    //decrease agents bal
    city[i][j].network[k].decrement_bal(amount);
    
    //record transaction
    Transaction transaction=Transaction(step, ageA, typeA, cityA, WITHDRAW, -amount, balA);
    transaction.is_suspicious();
    records.push_back(transaction);
}


//accept transfer - transaction log already taken care of
void accept_transfer(Message message, std::vector<std::vector<Network> > &city){
    
    //transaction details
    int i=message.i;
    int j=message.j;
    int k=message.k;
    double amount=message.amount;
    
    //decrease agents bal
    city[i][j].network[k].increment_bal(amount);
}



